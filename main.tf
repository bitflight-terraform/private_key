variable "key_name" {}
variable "output_path" {}

variable "s3_bucket_path" {
  default = ""
}

#Generate key for minions minions to use
resource "tls_private_key" "main" {
  algorithm = "RSA"
}

#push key to AWS
resource "aws_key_pair" "main" {
  key_name   = "${var.key_name}"
  public_key = "${tls_private_key.main.public_key_openssh}"

  depends_on = ["tls_private_key.main"]
}

output "public_key" {
  value = "${tls_private_key.main.public_key_pem}"
}

output "public_key_openssh" {
  value = "${tls_private_key.main.public_key_openssh}"
}

output "private_key" {
  value = "${tls_private_key.main.private_key_pem}"
}

output "key_name" {
  value = "${var.key_name}"
}
